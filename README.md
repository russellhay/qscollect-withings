qscollect-withings
==================

Collects weight measurements for withings.  Connects once a day to collect
the weight measurements

configuration
-------------

qscollect-withings uses the qscollect configuration tool.  Run

`qsconfig withings`

And you will be prompted for what to do.
